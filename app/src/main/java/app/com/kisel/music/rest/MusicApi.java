package app.com.kisel.music.rest;

import java.util.List;

import app.com.kisel.music.model.Model;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;


public interface MusicApi {
    //Базовая ссылка
    String BASE_URL = "http://cache-stav03.cdn.yandex.net/";


    //инцилизируем нашу библиотеку ретрофит 2,для работы с интернетом
    Retrofit retrofit = new Retrofit.Builder()
            //указываем ссылку
            .baseUrl(BASE_URL)
            //добавляем кастомный okttp3 клиент
            .client(new OkHttpClient())
            //добавляем конвертер (сериализация/десериализация)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

    //гет запрос,т.е получение чего-то,в данном случае music api
    @GET("download.cdn.yandex.net/mobilization-2016/artists.json")
    //метод на получение api
    Call<List<Model>> getMusicList();

}
