package app.com.kisel.music.adapter;


import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.vstechlab.easyfonts.EasyFonts;

import java.util.ArrayList;
import java.util.Collection;

import app.com.kisel.music.R;
import app.com.kisel.music.model.Model;
import app.com.kisel.music.utils.Utils;

public class MusicAdapter extends RecyclerView.Adapter<MusicAdapter.CardViewHolder> {

    //создаем массив из нашей модельки
    private ArrayList<Model> models;
    //контекст,для Fresco и т.д
    Context context;
    //конструктор
    public MusicAdapter(Context context) {
        models = new ArrayList<>();
        this.context = context;
    }

    //статичный класс,где мы находим наши виджеты из макета
    public static class CardViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        SimpleDraweeView photo;
        TextView genres;
        TextView track_album;
        TextView id_name;

        public CardViewHolder(final View item) {
            super(item);
            this.name = (TextView) item.findViewById(R.id.name);
            this.photo = (SimpleDraweeView) item.findViewById(R.id.photo);
            this.genres = (TextView) item.findViewById(R.id.genres);
            this.track_album = (TextView) item.findViewById(R.id.track_album);
            this.id_name = (TextView) item.findViewById(R.id.id_name);
        }
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //инцилизируем библиотеку по работе с кратинками и находим наш макет
        Fresco.initialize(context);
        View v = LayoutInflater.from(context)
                .inflate(R.layout.item_view, parent, false);
        return new CardViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        //выводим на экран все что получили

        //инцилизируем нашу модель
        Model item = models.get(position);
        String genres = "";
        for (int i = 0; i < item.getGenres().size(); i++) {
            genres += item.getGenres().get(i);
            if (i != item.getGenres().size() - 1)
                genres += ", ";
        }

        final String mid = context.getString(R.string.id) + " " + item.getId();

        //помещаем наши значения из модели в переменные,так же добавляем кастомные шрифты
        holder.id_name.setTypeface(EasyFonts.walkwayUltraBold(context));
        holder.id_name.setText(mid);
        holder.name.setTypeface(EasyFonts.caviarDreams(context));
        holder.name.setText(item.getName());
        holder.track_album.setText(Utils.getCountsString(item.getAlbums(), item.getTracks()));
        holder.genres.setText(genres);
        holder.track_album.setTypeface(EasyFonts.captureIt(context));
        holder.genres.setTypeface(EasyFonts.ostrichBlack(context));
        holder.photo.setImageURI(Uri.parse(item.getCover().get("small")));
    }


    public Model getItem(int position) {
        return models.get(position);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    //кастомный метод,для добавления массива записей
    public void Pagination(@NonNull Collection collection) {
        int curSize = getItemCount();
        models.addAll(collection);
        notifyItemRangeInserted(curSize, getItemCount());
    }
}