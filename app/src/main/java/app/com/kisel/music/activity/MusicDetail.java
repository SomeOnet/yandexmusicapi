package app.com.kisel.music.activity;

import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.vstechlab.easyfonts.EasyFonts;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import app.com.kisel.music.R;
import app.com.kisel.music.model.Model;
import app.com.kisel.music.utils.Utils;

//Находим наш макет
@EActivity(R.layout.music_detail)
public class MusicDetail extends AppCompatActivity {

    //инцилизируем наши виджеты
    @ViewById(R.id.music_image_view)
    SimpleDraweeView imageView;

    @ViewById(R.id.detail_track_album)
    TextView detail_track_album;


    @ViewById(R.id.detail_genre)
    TextView detail_genre;

    @ViewById(R.id.detail_desription)
    TextView detail_description;

    @ViewById(R.id.main_toolbar)
    Toolbar toolbar;

    @ViewById(R.id.id_music)
    TextView id_music;


    @AfterViews
    public void afterViews() {
        //инцилизируем для работы с картинкми из интернета,нашу библиотеку
        Fresco.initialize(getApplicationContext());
        //получить,при помощи сериализации,наши переменные
        Model model = (Model)getIntent().getSerializableExtra("example");
        //алгоритм для удаления боковых скобок из json файла,по значению жанр
        String genres = "";
        for (int i = 0; i < model.getGenres().size(); i++) {
            genres += model.getGenres().get(i);
            if (i != model.getGenres().size() - 1)
                genres += ", ";
        }

        //наше id значение
        final String mid = getApplicationContext().getString(R.string.id) + " " + model.getId();
        id_music.setText(mid);
        imageView.setImageURI(Uri.parse(model.getCover().get("big")));
        detail_track_album.setText(Utils.getCountsString(model.getTracks(),model.getAlbums()));
        //кастомные шрифты
        detail_genre.setTypeface(EasyFonts.caviarDreamsBold(getApplicationContext()));
        detail_track_album.setTypeface(EasyFonts.caviarDreams(getApplicationContext()));
        id_music.setTypeface(EasyFonts.caviarDreamsBold(getApplicationContext()));
        detail_description.setTypeface(EasyFonts.walkwaySemiBold(getApplicationContext()));
        detail_genre.setText(genres);
        detail_description.setText(model.getDescription());
        //прикрепляем тулбар
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            ActionBar actionBar = getSupportActionBar();
            //показать кнопку
            actionBar.setDisplayHomeAsUpEnabled(true);
            //чтобы по кнопке перейти в предыдущее активити
            actionBar.setHomeButtonEnabled(true);
            //помещаем в тулбар имя исполнителя
            getSupportActionBar().setTitle(model.getName());
        }
    }

    //завершаем активити,по нажатии на стрелку,в тулбаре
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {

            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
