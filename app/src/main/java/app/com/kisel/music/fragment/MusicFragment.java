package app.com.kisel.music.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.androidannotations.annotations.EFragment;

import java.util.List;

import app.com.kisel.music.BuildConfig;
import app.com.kisel.music.R;
import app.com.kisel.music.activity.MusicDetail_;
import app.com.kisel.music.adapter.MusicAdapter;
import app.com.kisel.music.model.Model;
import app.com.kisel.music.rest.MusicApi;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//находим наш макет
@EFragment(R.layout.music_fragment)
public class MusicFragment extends Fragment {
    private static final String TAG = "MainFragment";
    private static final boolean D = BuildConfig.DEBUG;

    RecyclerView recyclerView;
    private MusicAdapter musicAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initFragment(view);
    }


    private void initFragment(View view) {
        //находим наш ресайклер
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        //инцилизируем адаптер
        musicAdapter = new MusicAdapter(getContext());
        //кидаем адаптер в наш rv
        recyclerView.setAdapter(musicAdapter);
        //указываем,что в рв будет по 1 записи
        GridLayoutManager gridLayoutManager = new GridLayoutManager(view.getContext(), 1);
        //добавляем его в rv
        recyclerView.setLayoutManager(gridLayoutManager);
        //метод,который грузит наш json,ассинхронно
        getMusicFromI();
        //при ножатии на элемент списка в rv,открываем детализацию
        //ретролямбда для укорачивания однотипного кода
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(view.getContext(), (view1, position) -> {
            //инцилизируем bundle
            Bundle bundle = new Bundle();
            //получаем элементы по позициям из адаптера
            Model model = musicAdapter.getItem(position);
            //кидаем модельку с айди,чтобы отправить в детальный экран
            bundle.putSerializable("example", model);
            //стартуем явный интент,и кидаем наши переменные
            startActivity(new Intent(getContext(), MusicDetail_.class).putExtras(bundle));
        }));

    }

    private void getMusicFromI(){
        //инцилизируем наш API класс по работе с интернетом
        final MusicApi musicApi = MusicApi.retrofit.create(MusicApi.class);
        //ассинхронная загрузка Json файлов
        musicApi.getMusicList().enqueue(new Callback<List<Model>>() {
            //если загрузилось
            @Override
            public void onResponse(Call<List<Model>> call, Response<List<Model>> response) {
                //итак,пытаемся загрузить,если загрузилось > 0 данных то закидываем в адаптер и в последствии выводим
                try {
                    if (D) Log.d(TAG, "onResponse: " + response.body().toString());
                    //если > 0 данных
                    if (response.body().size() > 0) {
                        musicAdapter.Pagination(response.body());
                    }
                    //ну а если пришло Null то вот нам и ошибка соответствующая :,(
                } catch (NullPointerException ex) {
                    if (D) Log.e(TAG, "Null Body response\n" + ex.getMessage());
                }
            }

            //если не получилось связаться с json файлом,то выведем,что нет интернета,ведь его и нет,раз вы видите это сообщение :3
            @Override
            public void onFailure(Call<List<Model>> call, Throwable t) {
                if (D) Log.e(TAG, "onFailure: " + t.toString());
                if (isAdded()) { /* Check the user did not quit before updating on UI */
                    Toast.makeText(getContext(), getResources().getString(R.string.error_serverfailure), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
