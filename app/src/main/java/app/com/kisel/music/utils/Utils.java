package app.com.kisel.music.utils;

import java.util.ArrayList;

//класс для добавления исходя из значения песен и альбовом,соответствующих русскому языку слов
public class Utils {
    public static String getCountsString(int tracksCount, int albumsCount) {
        String resultString;
        ArrayList<Character> list = new ArrayList<Character>() {{
            add('2');
            add('3');
            add('4');
        }};

        String tracks = "" + tracksCount;
        String albums = "" + albumsCount;

        //проверка на значения и выставления соответствующих слов
        if ( ((11 > albumsCount) || (albumsCount > 20)) && albums.charAt(albums.length() - 1 ) == '1'){
            resultString = albumsCount + " альбом, ";
        } else if (((11 > albumsCount) || (albumsCount > 20)) && list.contains(albums.charAt(albums.length() - 1 ))) {
            resultString = albumsCount + " альбома, ";
        } else {
            resultString = albumsCount + " альбов, ";
        }
        String tempString;
        if ( (( 11 > tracksCount || tracksCount > 20)) && tracks.charAt(tracks.length() - 1 ) == '1'){
            tempString = tracksCount + " песня";
        } else if ((( 11 > tracksCount || tracksCount > 20)) && list.contains(tracks.charAt(tracks.length() - 1 ))) {
            tempString = tracksCount + " песни";
        } else {
            tempString = tracksCount + " песен";
        }
        resultString += tempString;
        return resultString;
    }

}
