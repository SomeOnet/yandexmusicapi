package app.com.kisel.music;

import android.annotation.TargetApi;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Fade;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import app.com.kisel.music.fragment.MusicFragment_;


//находим наш макет
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @AfterViews
    public void afterViews() {
        setToolbar();
        setupWindowAnimations();
        //добавляем наш фрагмент
        getSupportFragmentManager().beginTransaction().add(R.id.flContent, new MusicFragment_()).commit();
    }
    //анимация для появления активити
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setupWindowAnimations() {
        Fade fade = new Fade();
        fade.setDuration(1000);
        getWindow().setEnterTransition(fade);
    }

    @SuppressWarnings("ConstantConditions")
    private void setToolbar() {
        //присваиваем тулбар
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            //цвет для записи в тулбаре
            toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        }
    }
}
