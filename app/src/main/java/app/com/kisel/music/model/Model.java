package app.com.kisel.music.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


//наша модель,используем сериализацию для передачи данных между объектами
public class Model implements Serializable {
    //переменные
    private Map<String, String> cover;
    private List<String> genres;
    private String name;
    private int tracks;
    private int albums;
    private String description;
    private String id;

    //гетеры и сеттеры
    public Map<String, String> getCover() {
        return cover;
    }

    public void setCover(Map<String, String> cover) {
        this.cover = cover;
    }

    public List<String> getGenres() {
        return genres;
    }

    public void setGenres(List<String> genres) {
        this.genres = genres;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAlbums() {
        return albums;
    }

    public void setAlbums(int albums) {
        this.albums = albums;
    }

    public int getTracks() {
        return tracks;
    }

    public void setTracks(int tracks) {
        this.tracks = tracks;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
